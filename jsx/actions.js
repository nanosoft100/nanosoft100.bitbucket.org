import constants from './constants';

export default{
 set_members(data) {
    return {
      type: constants.set_members,
      payload: data
    };
  },

   set_suppliers(data) {
    return {
      type: constants.set_suppliers,
      payload: data
    };
  },
}