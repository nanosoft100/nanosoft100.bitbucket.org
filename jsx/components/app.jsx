import React, { Component } from 'react';
import cookie from 'react-cookie';
import { bindActionCreators } from 'redux';
import Actions from '../actions';
import { connect } from 'react-redux';

class App extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {


    }

    handlePrint() {
        let doc = new jsPDF()
        doc = new jsPDF({
            orientation: "portrait",
            unit: 'mm'

        })

        doc.text('Hello world!', 10, 10);
        doc.addPage();

        doc.text('Hello world!', 10, 10);

      //  doc.save('two-by-four.pdf');

        document.getElementById('pdf_preview').src = doc.output('datauristring');
        document.getElementById("backdrop").className = "modal-backdrop fade in";
        document.getElementById("myModal").className = "modal fade in";
        document.getElementById("myModal").style.display = "block";
        document.getElementsByTagName("body")[0].className  = "modal-open";
    }

    handleClose() { 
         document.getElementById("backdrop").className = "";
        document.getElementById("myModal").className = "modal fade";
        document.getElementById("myModal").style.display = "none";
        document.body.removeAttribute("class");
        
    }

    handleDelete(i) {
        let _members = Object.assign([], this.props.members);
        _members.splice(i, 1);
        this.props.set_members(_members);
    }

    handleDeleteSupplier(i){
        let _members = Object.assign([], this.props.suppliers);
        _members.splice(i, 1);
        this.props.set_suppliers(_members);
    }


    render() {
        return (
            <div className="table-responsive">
                <table className="display table table-striped dataTable">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            this.props.members.map((item, i) =>
                                (
                                    <tr key={i}>
                                        <td>{item.name}</td>
                                        <td></td>
                                        <td>
                                            <a href="#" className="btn btn-danger" onClick={this.handleDelete.bind(this, i)}>Delete</a>
                                        </td>
                                    </tr>
                                ))
                        }
                    </tbody>
                </table>

                <table className="display table table-striped dataTable">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Address</th>
                            <th>Email</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            this.props.suppliers.map((item, i) =>
                                (
                                    <tr key={i}>
                                        <td>{item.name}</td>
                                        <td>{item.address}</td>
                                        <td>{item.email}</td>
                                        <td>
                                            <a href="#" className="btn btn-danger" onClick={this.handleDeleteSupplier.bind(this, i)}>Delete</a>
                                        </td>
                                    </tr>
                                ))
                        }
                    </tbody>
                </table>

                <button className="btn btn-success" onClick={this.handlePrint.bind(this)}>Print</button>
                

                <div id="myModal" className="modal fade" role="dialog">
                    <div className="modal-dialog">

                        <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" onClick={this.handleClose.bind(this)} className="close" data-dismiss="modal">&times;</button>
                                <h4 className="modal-title">Modal Header</h4>
                            </div>
                            <div className="modal-body">
                               <iframe id="pdf_preview" type="application/pdf" src="" width="100%" height="400" />
                            </div>
                            <div className="modal-footer">
                                <button type="button" onClick={this.handleClose.bind(this)} className="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        members: state.members,
        suppliers: _.orderBy(state.suppliers, ["name"])
    }
}

const matchDispatchToProps = (dispatch) => {
    return bindActionCreators({
        set_members: Actions.set_members,
        set_suppliers: Actions.set_suppliers
    }, dispatch);
}

export default connect(mapStateToProps, matchDispatchToProps)(App)
