"use strict";
import {
    createStore,
    applyMiddleware,
    combineReducers
} from 'redux';
import createLogger from 'redux-logger';
import thunk from 'redux-thunk';
import constants from './constants';
import SupplierData from "./sampledata/suppliers"

const logger = createLogger();


const initMembers = [
    {name: "Paul"},
    {name: "John"},
    {name: "Jim"},
    {name: "Peter"},
    {name: "Jack"}
]

function members(state = initMembers, action) {
    switch (action.type) {

        case constants.set_members:
            return action.payload;

        default:
            return state;
    }
}

function suppliers(state = SupplierData, action){
    switch(action.type){
        case constants.set_suppliers:
            return action.payload;

        default:
            return state;
    }
}



let reducer = combineReducers({
    members,
    suppliers
});

let store = createStore(
    reducer,
    applyMiddleware(thunk, logger)
);

export default store;