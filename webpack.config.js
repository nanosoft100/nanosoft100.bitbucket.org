var path = require('path');
var webpack = require('webpack');

module.exports = {
  entry: {
    index: "./jsx/index"
  },
  output: {
    path: path.resolve('./public/dist/bundles'),
    filename: "bundle.js",
    publicPath: '/dist/bundles/'
  },
  devtool: 'source-map',
  module: {
    loaders: [{
      test: /.jsx?$/,
      loader: 'babel-loader',
      exclude: /node_modules/,
      query: {
        presets: ['es2015', 'react']
      }
    },
      {
        test: /\.css$/,
        loader: 'style!css!'
      },
    ]
  },
   externals: {
    //don't bundle the 'react' npm package with our bundle.js
    //but get it from a global 'React' variable
    'React': 'React'
  },
  resolve: {
    extensions: ['', '.js', '.jsx']
  }
};
